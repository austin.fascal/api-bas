const admin = require('firebase-admin');

const credentials = require("./permissions.json");

const db = admin.initializeApp({
  credential: admin.credential.cert(credentials),
  databaseURL: "https://bas-control-center-2-default-rtdb.asia-southeast1.firebasedatabase.app"
});

module.exports = db;