'use strict';

require('dotenv').config();

const firebase = require('./db');
const firestore = firebase.firestore();

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const model = "CashOutData";
const recapDataModel = "RecapData";

const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.json());


app.get('/:id', authenticateToken, async (req, res)=>{
    try {
        const id = req.params.id;
        const cashOutData = await firestore.collection(model).doc(id);
        const data = await cashOutData.get();
        if(!data.exists) {
            res.status(404).send('CashOut with the given ID not found');
        }else {
            res.send(data.data());
        }
    } catch (error) {
        res.status(400).send(error.message);
    }
});

app.put('/:id', authenticateToken, async (req, res)=>{
    try {
        const id = req.params.id;
        const data = req.body;
        const cashOutData =  await firestore.collection(model).doc(id);
        await cashOutData.update(data);
        res.send('CashOut record updated successfuly');        
    } catch (error) {
        res.status(400).send(error.message);
    }
});

app.delete('/:id', authenticateToken, async (req, res)=>{
    try {
        const id = req.params.id;
        await firestore.collection(model).doc(id).delete();
        const batch = firestore.batch();
        const querySnapshot = await firestore.collection(recapDataModel).where("rcpGiCoUID", "==", id).get();
        querySnapshot.forEach((doc) => {
            const docRef = firestore.collection(recapDataModel).doc(doc.id);
            batch.update(docRef, { rcpGiCoUID: "" });
        });
        await batch.commit();
        res.send('CashOut record deleted successfuly');
    } catch (error) {
        res.status(400).send(error.message);
    }
});

app.use('/auth', (req, res) =>{
    const accessKey = req.body.accessKey;
    const access = {name: accessKey};
    const accessToken = jwt.sign(access, process.env.ACCESS_TOKEN_SECRET);
    res.json({accessToken: accessToken});
});

function authenticateToken(req, res, next){    
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if(token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, access) =>{
        if(err) return res.sendStatus(403);
        req.access = access;
        next();
    })
}


var server = app.listen(process.env.PORT, function() {
  console.log('Ready on port %d', server.address().port);
});